% Project 2: Task 3 (2D Transient Heat Transfer)
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%
% To perform the finite element analysis of a 2D transient heat transfer
% problem.
%
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

clear variables; clc;
close all;

%% define general parameters
% define wall material thickness
h = [0.4 0.2 0.018];

% define material thermal conductivities
k = [1.7 0.04 0.22];

% define convective boundary condition data
Tamb = [25, -5];
alpha = [8, 25];

% number of nodal Dof
NoNodalDofs = 1;

% define length of the wall (away from thermal bridge)
% -- NOTE -----------------------------------------------------------------
% this should be the same as the length when exporting mesh from COMSOL
% -------------------------------------------------------------------------
L = 3.5; %m

% define heat capacity of materials
cp = [0.96 0.84 0.9]*1000;
%cp = [960, 840, 900];

% define density of materials
rho = [2300, 144, 849];

%% import 2D mesh from COMSOL
% this uses pre-defined MatLAB routines

% define default values to use in COMSOL2CALFEM_heat2D
meshFileName = 'task2_normal_v3.mphtxt';

% define whether plots should be displayed
plotMesh = 'no';
plotBounds = 'no';

% convert COMSOL mesh to CALFEM mesh
[Coord, Edof, Dof, Ex, Ey, matrlIndex, boundaryEdof, boundaryEx, ...
    boundaryEy, boundaryMaterial] = COMSOL2CALFEM_heat2D(meshFileName, ...
    1, h(1), h(2), h(3), L, plotMesh, plotBounds);

%% define loading data
Q = 0;   % NO external heat supply
eq = Q;

%% define unit height of wall
thick = 1;

%% initialize matrices
K = zeros(size(Dof, 1));        % global stiffness matrix, K
fl = zeros(size(Dof, 1), 1);    % load vector, fl

%% calculate the element stiffness matrix and assemble into K
[~, n] = size(Edof); 
for i = 1:length(Edof)
    % determine the thermal conductivity of the element, i
    kk = k(matrlIndex(i,2));
    D = [kk 0; 0 kk];
    
    % determine the element stiffness matrix and load vector
    [Ke, fe] = flw2te(Ex(i,:), Ey(i, :), thick, D, eq);
    
    % assemble Ke into K
    [K, fl] = assem(Edof(i, :), K, Ke, fl, fe);
end

%% calculate the convective element stiffness matrix into Kc
[Kc, fb0] = convectiveKc(boundaryEdof, boundaryMaterial, boundaryEx, ...
                        boundaryEy, Tamb, alpha, thick, Dof);

%% combine K1 and Kc
K = K + Kc;

%% solve steady-state problem
T0 = K\fb0;

ed0 = extract(Edof, T0);
                    
%% calculate element capacity matrix and assemble into elemCapMat
elemType = 'triangular';
NoIntPoints = 3;
CapMat = zeros(size(Dof, 1));

for i = 1:length(Edof)
    
    const = cp(matrlIndex(i, 2)) * rho(matrlIndex(i, 2));
    
    % determine the element capacity matrix
    elemCapMat = flw2ieCap( elemType, Ex(i, :), Ey(i, :), ...
                            const, NoIntPoints);
    
    % assemble elemCapMat into CapMat
    CapMat = assem(Edof(i, :), CapMat, elemCapMat);
end

%% solution to the transient problem
tend = 5*3600; % 5 hours converted to seconds

% recompute the convective boundary condition using new indoor
% ambient air temperature
Tambnew = [10, -5];
[~, fbnew] = convectiveKc(boundaryEdof, boundaryMaterial, boundaryEx, ...
                        boundaryEy, Tambnew, alpha, thick, Dof);

%% subtask c
%some node (bottom right corner)
tf = (boundaryEx(:,1) == 0.618) & (boundaryEy(:,1) == 0.0);
dof = boundaryEdof(tf, 2);

% define method: Forward Euler
theta = 0;

% determine max eigenvalue
ev = eigen(K, CapMat);
dt_crit = round(2/(max(ev)));
fprintf('The critical time step for the Forward Euler Method is: %.2fs\n', ...
        dt_crit);

dt = linspace(5, dt_crit, 25); %round(dt_crit) - 5; % 

% define figure to plot in
figure(1);
hold on;
sgtitle("Temperature Distribution as a function of timestep");

for tt = 1:length(dt)
    steps = round(tend/dt(tt));
    psteps = [round(steps/3), round(2*steps/3), steps];

    % define previous temperature
    Tfe = zeros(size(K, 1), steps);
    Tfe(:, 1) = T0;

    % time stepping
    Ktheta = (CapMat + dt(tt) * theta * K);
    Tfe = timestepping_conv(Tfe, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                       Tamb, dt(tt), theta, steps, psteps);
                   
    timesteps(tt) = steps;         
    temp5hr(tt) = Tfe(dof, end);               
end

% plotting
plot(timesteps, temp5hr, 'LineWidth', 2);

% define method: Backward Euler
theta = 1;
dt = linspace(5, dt_crit, 25); 

for tt = 1:length(dt)
    steps = round(tend/dt(tt));
    psteps = [round(steps/3), round(2*steps/3), steps];

    % define previous temperature
    Tbe = zeros(size(K, 1), steps);
    Tbe(:, 1) = T0;

    % time stepping & plotting
    Ktheta = (CapMat + dt(tt) * theta * K);
    Tbe = timestepping_conv(Tbe, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                       Tamb, dt(tt) , theta, steps, psteps);
                   
    timesteps(tt) = steps;         
    temp5hr(tt) = Tbe(dof, end);
end

% plotting
plot(timesteps, temp5hr, 'LineWidth', 2);

% define method: Crank-Nicholson
theta = 0.5;
dt = linspace(5, dt_crit, 25); % 

for tt = 1:length(dt)
    steps = round(tend/dt(tt));
    psteps = [round(steps/3), round(2*steps/3), steps];

    % define previous temperature
    Tcn = zeros(size(K, 1), steps);
    Tcn(:, 1) = T0;

    % time stepping & plotting
    Ktheta = (CapMat + dt(tt) * theta * K);
    Tcn = timestepping_conv(Tcn, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                       Tamb, dt(tt), theta, steps, psteps);
    
    timesteps(tt) = steps;         
    temp5hr(tt) = Tcn(dof, end);               
end

% plotting
plot(timesteps, temp5hr, 'LineWidth', 2);
legend('FE','BE', 'CN');
hold off;
savefig(1, sprintf('subtaskC_%s',meshFileName(1:end-7)));
saveas(1, sprintf('subtaskC_%s',meshFileName(1:end-7)), 'epsc')

%% subtask d

% define method: Forward Euler
theta = 0;

% determine max eigenvalue
ev = eigen(K, CapMat);
dt_crit = round(2/(max(ev)));
fprintf('The critical time step for the Forward Euler Method is: %.2fs\n', ...
        dt_crit);

dt = round(dt_crit) - 5; % 

% define figure to plot in
figure(2);
hold on;
sgtitle("Temperature Distribution");

steps = round(tend/dt);
psteps = [round(steps/3), round(2*steps/3), steps];

% define previous temperature
Tfe = zeros(size(K, 1), steps);
Tfe(:, 1) = T0;

% time stepping
Ktheta = (CapMat + dt  * theta * K);
Tfe = timestepping(Tfe, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                   Tamb, dt , theta, steps, psteps);

savefig(2, sprintf('./imgs/forwardeuler-%s.fig',meshFileName(1:end-7)));
saveas(2, sprintf('./imgs/forwardeuler-%s',meshFileName(1:end-7)), 'epsc');               
               
% define method: Backward Euler
theta = 1;
dt = 1500;

% define figure to plot in
figure(3);
hold on;
sgtitle("Temperature Distribution");

steps = round(tend/dt );
psteps = [round(steps/3), round(2*steps/3), steps];

% define previous temperature
Tbe = zeros(size(K, 1), steps);
Tbe(:, 1) = T0;

% time stepping & plotting
Ktheta = (CapMat + dt  * theta * K);
Tbe = timestepping(Tbe, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                   Tamb, dt  , theta, steps, psteps);

savefig(3, sprintf('./imgs/backwardeuler-%s.fig',meshFileName(1:end-7)));
saveas(3, sprintf('./imgs/backwardeuler-%s',meshFileName(1:end-7)), 'epsc');               
               
% define method: Crank-Nicholson
theta = 0.5;
dt = 1500;

% define figure to plot in
figure(4);
hold on;
sgtitle("Temperature Distribution");

steps = round(tend/dt );
psteps = [round(steps/3), round(2*steps/3), steps];

% define previous temperature
Tcn = zeros(size(K, 1), steps);
Tcn(:, 1) = T0;

% time stepping & plotting
Ktheta = (CapMat + dt  * theta * K);
Tcn = timestepping(Tcn, Ktheta, CapMat, K, fbnew, Edof, Ex, Ey, ...
                   Tamb, dt , theta, steps, psteps);
    
savefig(4, sprintf('./imgs/cranknicholson-%s.fig',meshFileName(1:end-7)));
saveas(4, sprintf('./imgs/cranknicholson-%s',meshFileName(1:end-7)), 'epsc');

%% Plotting
tauFe = linspace(0, 5*3600, size(Tfe, 2))';
tauBe = linspace(0, 5*3600, size(Tbe, 2))';
tauCn = linspace(0, 5*3600, size(Tcn, 2))';

figure(5);
hold on;
plot(tauFe, Tfe(dof, :), 'LineWidth', 2);
plot(tauBe, Tbe(dof, :), 'LineWidth', 2);
plot(tauCn, Tcn(dof, :), 'LineWidth', 2);
legend('FE','BE', 'CN');
xlabel('Time [s]');
ylabel('Temperature [C]');
title(sprintf('Temperature variation as a function of time for dof = %d @ x = 0.618m and y = 0m',dof));
savefig(5, sprintf('./imgs/compare3methods_%d_%s.fig', dof, meshFileName(1:end-7)));
saveas(5, sprintf('./imgs/compare3methods_%d_%s', dof, meshFileName(1:end-7)), 'epsc');