function [ Coord, Edof, Dof, Ex, Ey, matrlIndex, boundaryEdof, boundaryEx , boundaryEy, boundaryMaterial] = COMSOL2CALFEM_heat2D( meshFileName, NoNodalDofs, h1, h2, h3, L, plotMesh, plotBounds )
% function [ Coord, Edof, Dof, Ex, Ey, boundaryEdof_out           , ...
%            boundaryEx_out, boundaryEy_out, boundaryEdof_in      , ...
%            boundaryEx_in, boundaryEy_in ] = COMSOL2CALFEM_heat2D( ...
%            meshFileName, NoNodalDofs, h1, h2, h3, h4, L2, L1, H , ...
%            plotMesh )
%-------------------------------------------------------------------------
% Purpose: Compute the CALFEM matrices necessary for an FE-analysis, 
%          given a COMSOL text (.mphtxt) file with nodal and element 
%          data. The boundary matrices are also computed for the specific
%          heat problem of a connection between a wall and a slab.
%-------------------------------------------------------------------------
% Input: meshFileName  Name of COMSOL text (.mphtxt) file (you should 
%                      provide the text file name with the file extension)
%        NoNodalDofs   Number of degrees-of-freedom (dof) per node
%        h1            Thickness of concrete layer
%        h2            Thickness of insulation layer
%        h3            Thickness of plaster layer
%        L             Inner wall length (see assignment)
%        plotMesh      plotMesh   = 'yes', if you want to plot the mesh
%        plotBounds    plotBounds = 'yes', if you want to plot the
%                      boundaries
%-------------------------------------------------------------------------
% Output: Coord        Global nodal coordinates matrix, 
%                      size = NoNodes x problemDim, problemDim = 2 or 3 
%         Edof         Topology matrix in terms of dof
%         Dof          Global nodal dof matrix, 
%                      size = NoNodes x NoNodalDofs
%         Ex           Element nodal x-coordinates, 
%                      size = NoElements x NoElemNodes
%         Ey           Element nodal y-coordinates, 
%                      size = NoElements x NoElemNodes
%         matrlIndex   Vector with indices for each element, depending
%                      on the material the element is made of,
%                      index = 1 for concrete, 
%                      index = 2 for insulation,
%                      index = 3 for plaster
%         boundaryEdof Topology matrix of boundary segments,
%                      boundaryEdof( e, : ) = [ e elDof_x elDof_y ]
%         boundaryEx   Boundary segment nodal x-coordinate matrix,
%                      boundaryEx( e, : ) = [ elCoord_x1 elCoord_x2 ]                       
%         boundaryEy   Boundary segment nodal y-coordinate matrix,
%                      boundaryEy( e, : ) = [ elCoord_y1 elCoord_y2 ]
%         boundaryMaterial  boundary segment indices
%                           boundaryType( e, 2 ) = 1, for inner boundary
%                           boundaryType( e, 2 ) = 2, for outer boundary
%                           boundaryType( e, 2 ) = 3, for lower boundary
%                           boundaryType( e, 2 ) = 4, for upper boundary
%      
%                            | \
%                            |  \
%                            |   \ 4
%                            |    \
%                            |     \
%                            |      \
%                            |       \
%                            |       |
%                            |       |
%                            |       |    
%                          2 |       | 1
%                            |       |    
%                            |       |
%                            |       | 
%                            |_______|
%                                3
%-----------------------------------------------------------------------
% Created by: Dimosthenis Floros, 20171017
% Modified by: Carolyn Oddy, 20181122
%-----------------------------------------------------------------------

% Compute the CALFEM matrices based on a COMSOL text file. This function
% is generic and it holds for all problems

[ Coord, Edof, Dof, Ex, Ey ] = COMSOL2CALFEM( meshFileName, ...
                                              NoNodalDofs, plotMesh );
                                  
% Compute the boundary segments matrices for application of boundary 
% conditions, given Coord, Dof. This function needs to be adjusted to 
% the boundaries of the domain we are solving in each different problem
               

[ boundaryEdof, boundaryEx, boundaryEy, boundaryMaterial  , ...
  matrlIndex ] = heat2DWallBoundaryData( Coord, Dof, Ex, Ey, ...
  h1, h2, h3, L, plotBounds );



end

