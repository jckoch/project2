function [ boundaryEdof, boundaryEx, boundaryEy, boundaryMaterial, matrlIndex ] = heat2DWallBoundaryData( Coord, Dof, Ex, Ey, h1, h2, h3, L, plotBounds)
% function [ boundaryEdof, boundaryEx, boundaryEy, boundaryMaterial, ...
%            matrlIndex ] = heat2DWallBoundaryData( Coord, Dof, Ex, Ey, ...
%            h1, h2, h3, h4, L2, L1, H, plotBounds )
%------------------------------------------------------------------------
% Purpose: Extract the topology and element nodal coordinates matrices
%          of the outer boundaries of the domain of a square plate
%-------------------------------------------------------------------------
% Input: Coord        Global nodal coordinates matrix, 
%                     size = NoNodes x problemDim, problemDim = 2 or 3
%        Dof          Global nodal dof matrix, 
%                     size = NoNodes x NoNodalDofs
%        Ex           Element nodal x-coordinates, 
%                     size = NoElements x NoElemNodes
%        Ey           Element nodal y-coordinates, 
%                     size = NoElements x NoElemNodes
%        h1           Thickness of concrete layer
%        h2           Thickness of insulation layer
%        h3           Thickness of plaster layer
%        h4           Full thickness of concrete slab
%        L2           Length of concrete slab exposed to outer temperature
%        L1           Length of concrete slab exposed to inner temperature
%        H            Total height of the wall
%        plotBounds   = 'yes', if you want to plot the boundaries
%-------------------------------------------------------------------------
% Output: boundaryEdof  Topology matrix of the boundary segments,
%                       boundaryEdof( e, : ) = [ e elDof_x elDof_y ]
%         boundaryEx    Boundary segment nodal x-coordinate matrix,
%                       boundaryEx( e, : ) = [ elCoord_x1 elCoord_x2 ]                       
%         boundaryEy    Boundary segment nodal y-coordinate matrix,
%                       boundaryEx( e, : ) = [ elCoord_y1 elCoord_y2 ]
%-----------------------------------------------------------------------
% Created by: Dimosthenis Floros, 20171017
% Modified by: Carolyn Oddy, 20181121
%-----------------------------------------------------------------------

%% Extract minimum coordinates of the domain

boundaryEdof = 1; boundaryEx=1; boundaryEy=1; boundaryMaterial =1; matrlIndex=1;

minXcoord = min( Coord( :, 1 ) );
minYcoord = min( Coord( :, 2 ) );

Apoint = [ minXcoord minYcoord ];
Bpoint = Apoint + [h1+h2+h3 0 ];
Cpoint = Bpoint + [0 L ];
Dpoint = Apoint + [0 (L+h1+h2+h3)];


%% Find the nodes that live at the outer boundaries of the plate

ABboundNodesTemp = find(( abs(Coord( :, 2 ) - Apoint(2)) < 10^(-10) ));
                   
BCboundNodesTemp = find((abs(Coord( :, 1 ) - Bpoint(1)) < 10^(-10)));               
                   
DAboundNodesTemp = find((abs(Coord(:,1) - Dpoint(1))<10^(-10)));
                         
                      
%equation of line between Cpoint and Dpoint
x1 = Dpoint(1);
y1 = Dpoint(2);
x2 = Cpoint(1);
y2 = Cpoint(2);

m = (y1-y2)/(x1-x2);
b = (x1*y2-x2*y1)/(x1-x2);

CDboundNodesTemp = find((abs(m*Coord(:,1)+b - Coord(:,2))<10^(-10)));

% %
 [ ~, ABboundNodesInd ] = ...
 sort( Coord( ABboundNodesTemp, 1 ), 'ascend' );
 [ ~, BCboundNodesInd ] = ...
    sort( Coord( BCboundNodesTemp, 2 ), 'ascend' );
 [ ~, CDboundNodesInd ] = ...
   sort( Coord( CDboundNodesTemp, 1 ), 'descend' );
 [ ~, DAboundNodesInd ] = ...
    sort( Coord( DAboundNodesTemp, 2 ), 'descend' );

% 
ABboundNodes = ABboundNodesTemp( ABboundNodesInd );
BCboundNodes = BCboundNodesTemp( BCboundNodesInd );
CDboundNodes = CDboundNodesTemp( CDboundNodesInd );
DAboundNodes = DAboundNodesTemp( DAboundNodesInd );

ABCDboundNodes = unique([ABboundNodes;BCboundNodes;CDboundNodes;DAboundNodes]);

boundNodes = ABCDboundNodes; 
                     
%% Compute boundary segments FE-matrices        
%       
 NoBoundSegments = length(boundNodes);                                                       
 NoNodalDofs = size( Dof, 2 );
 
 boundaryEdof = zeros(length(ABCDboundNodes),3);
 boundaryMaterial = zeros(length(ABCDboundNodes),2);
 boundaryEx = zeros(length(ABCDboundNodes),2);
 boundaryEy = zeros(length(ABCDboundNodes),2);

 counter_1 = 0;
 counter_2 = 0;
 counter_3 = 0;
  for segmIndex = 1:NoBoundSegments
      
      
      boundaryEdof(segmIndex,1) = segmIndex;
      boundaryType(segmIndex,1) = segmIndex;
      %for lower boundary
      if segmIndex <= length(ABboundNodes)-1
          boundaryEdof(segmIndex,2) = ABboundNodes(segmIndex) ;
          boundaryEdof(segmIndex,3) = ABboundNodes(segmIndex+1);
          boundaryMaterial(segmIndex,1) = segmIndex;
          boundaryMaterial(segmIndex,2) = 3;
          boundaryEx(segmIndex,:) = [Coord(ABboundNodes(segmIndex),1) Coord(ABboundNodes(segmIndex+1),1)];
          boundaryEy(segmIndex,:) = [Coord(ABboundNodes(segmIndex),2) Coord(ABboundNodes(segmIndex+1),2)];
          
     %for right hand boundary
      elseif segmIndex<=(length(ABboundNodes)+length(BCboundNodes)-2)    
         counter_1 = counter_1 +1;
         boundaryEdof(segmIndex,2) = BCboundNodes(counter_1);
         boundaryEdof(segmIndex,3) = BCboundNodes(counter_1+1); 
         boundaryMaterial(segmIndex,1) = segmIndex;
         boundaryMaterial(segmIndex,2) = 1;
         boundaryEx(segmIndex,:) = [Coord(BCboundNodes(counter_1),1) Coord(BCboundNodes(counter_1+1),1)];
         boundaryEy(segmIndex,:) = [Coord(BCboundNodes(counter_1),2) Coord(BCboundNodes(counter_1+1),2)];
    
      %for symmetirc boundary
      elseif segmIndex<=(length(ABboundNodes)+length(BCboundNodes)+length(CDboundNodes)-3)
         counter_2 = counter_2+1;
         boundaryEdof(segmIndex,2) = CDboundNodes(counter_2);
         boundaryEdof(segmIndex,3) = CDboundNodes(counter_2+1); 
         boundaryMaterial(segmIndex,1) = segmIndex;
         boundaryMaterial(segmIndex,2) = 4;
         boundaryEx(segmIndex,:) = [Coord(CDboundNodes(counter_2),1) Coord(CDboundNodes(counter_2+1),1)];
         boundaryEy(segmIndex,:) = [Coord(CDboundNodes(counter_2),2) Coord(CDboundNodes(counter_2+1),2)];
      
      %for left hand boundary
      else
         counter_3=counter_3+1;
         boundaryEdof(segmIndex,2) = DAboundNodes(counter_3);
         boundaryEdof(segmIndex,3) = DAboundNodes(counter_3+1);  
         boundaryMaterial(segmIndex,1) = segmIndex;
         boundaryMaterial(segmIndex,2) = 2;
         boundaryEx(segmIndex,:) = [Coord(DAboundNodes(counter_3),1) Coord(DAboundNodes(counter_3+1),1)];
         boundaryEy(segmIndex,:) = [Coord(DAboundNodes(counter_3),2) Coord(DAboundNodes(counter_3+1),2)];
     end
          
  end          
  
 
%% Plotting Boundary 
if strcmp( plotBounds, 'yes' )
    
    figure; 
    hold on; axis equal;
    for elemIndex = 1:NoBoundSegments
       
        plot( [ boundaryEx( elemIndex, : ) boundaryEx( elemIndex, 1 ) ]', ...
              [ boundaryEy( elemIndex, : ) boundaryEy( elemIndex, 1 ) ]', ...
              '-ko' );   
        text( mean( boundaryEx( elemIndex, : ) ), mean( boundaryEy( elemIndex, : ) ), ...
              num2str(elemIndex) );   
        
    end
    hold off;
    
end

%% Find material index depending on where we look in the domain

 
 NoElem = length(Ex);
 matrlIndex = zeros( NoElem, 2 );
 for elemIndex = 1:NoElem
    
     xMean = mean( Ex( elemIndex, : ) );
     matrlIndex(elemIndex,1) = elemIndex;
     % concrete 
     if xMean < h1

         matrlIndex( elemIndex,2 ) = 1;
      %insulation   
     elseif xMean > h1 && xMean < h1+h2
         
         matrlIndex( elemIndex,2 ) = 2;
      %plaster   
     elseif xMean > h1+h2 && xMean < h1+h2+h3
                             
         matrlIndex( elemIndex,2 ) = 3;
              
    end

end

