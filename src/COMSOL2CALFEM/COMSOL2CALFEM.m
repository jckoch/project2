function [ Coord, Edof, Dof, Ex, Ey ] = COMSOL2CALFEM( meshFileName, NoNodalDofs, plotMesh )
% function [ Coord, Edof, Dof, Ex, Ey ] = COMSOL2CALFEM( meshFileName, ..., NoNodalDofs )
%-------------------------------------------------------------------------
% Purpose: Compute the CALFEM matrices necessary for an FE-analysis, 
%          given a COMSOL text (.mphtxt) file with nodal and element 
%          data
%-------------------------------------------------------------------------
% Input: meshFileName  Name of COMSOL text (.mphtxt) file (you should 
%                      provide the text file name with the file extension)
%        NoNodalDofs   Number of degrees-of-freedom (dof) per node
%        plotMesh      plotMesh = 'yes', if you want to plot the mesh
%-------------------------------------------------------------------------
% Output: Coord        Global nodal coordinates matrix, 
%                      size = NoNodes x problemDim, problemDim = 2 or 3 
%         Edof         Topology matrix in terms of dof
%         Dof          Global nodal dof matrix, 
%                      size = NoNodes x NoNodalDofs
%         Ex           Element nodal x-coordinates, 
%                      size = NoElements x NoElemNodes
%         Ey           Element nodal y-coordinates, 
%                      size = NoElements x NoElemNodes
%-----------------------------------------------------------------------
% Created by: Dimosthenis Floros, 20171017
%-----------------------------------------------------------------------

%% Extract nodal coordinates matrix from COMSOL text file

seektheNodesString = '# Mesh point coordinates';
OccurVal           = 1; % Parameter set to 1 because the set 
                        % "seektheNodesString" appears only once in the 
                        % COMSOL text file

% Find the lines within which matrix Coord is situated in COMSOL text file                        
                        
[ ~, NoLineNodes, NoNextNSetNodes ] = findLines( meshFileName, ...
                                      seektheNodesString, OccurVal );

% Go through the COMSOL text file again and extract the data the live 
% between the lines defined by "findlines" function
                                  
Coord = findNodes( meshFileName, NoLineNodes, NoNextNSetNodes );

%% Extract topology matrix in terms of nodes from COMSOL text file

seekConnectivityMatrix = '# Elements';
OccurVal               = 3;

% Find the lines within which matrix Enode is situated in COMSOL text file                        

[ ~, NoLineElement, NoNextNSetElement ] = findLines( meshFileName, ...
  seekConnectivityMatrix, OccurVal );
                          
% Go through the COMSOL text file again and extract the data the live 
% between the lines defined by "findlines" function

EnodeTemp = findNodes( meshFileName, NoLineElement, NoNextNSetElement );

% EnodeTemp contains zeroth nodes, thus we transform it such that the 
% node with the smallest label is node 1

[ NoElem, NoElemNodes ] = size( EnodeTemp ); 

Enode = EnodeTemp + ones( NoElem, NoElemNodes ); 

% Tranform Enode such that element nodes are ordered anti-clockwise

if NoElemNodes == 3 % For linear triangles

    for elemIndex = 1:NoElem

        vector1 = [ Coord( Enode( elemIndex, 2 ), 1 ) - ...
                    Coord( Enode( elemIndex, 1 ), 1 );
                    Coord( Enode( elemIndex, 2 ), 2 ) - ...
                    Coord( Enode( elemIndex, 1 ), 2 ); 0 ];

        vector2 = [ Coord( Enode( elemIndex, 3 ), 1 ) - ...
                    Coord( Enode( elemIndex, 1 ), 1 );
                    Coord( Enode( elemIndex, 3 ), 2 ) - ...
                    Coord( Enode( elemIndex, 1 ), 2 ); 0 ];

        vector3 = cross( vector1, vector2 );

        if vector3(3) < 0

            Enode( elemIndex, [ end - 1, end ]  ) = ...
              flip( Enode( elemIndex, [ end - 1, end ]  ), 2 );

        end

    end

elseif NoElemNodes == 4 % For bi-linear quadrilaterals
    
    for elemIndex = 1:NoElem

        vector1 = [ Coord( Enode( elemIndex, 3 ), 1 ) - ...
                    Coord( Enode( elemIndex, 1 ), 1 );
                    Coord( Enode( elemIndex, 3 ), 2 ) - ...
                    Coord( Enode( elemIndex, 1 ), 2 ); 0 ];

        vector2 = [ Coord( Enode( elemIndex, 4 ), 1 ) - ...
                    Coord( Enode( elemIndex, 1 ), 1 );
                    Coord( Enode( elemIndex, 4 ), 2 ) - ...
                    Coord( Enode( elemIndex, 1 ), 2 ); 0 ];

        vector3 = cross( vector1, vector2 );

        if vector3(3) < 0

            Enode( elemIndex, [ end - 1, end ]  ) = ...
              flip( Enode( elemIndex, [ end - 1, end ]  ), 2 );

        end

    end    
        
end

% Delete temporary file created during text processing

folderPath = pwd;

tempTextFilePath = sprintf( '%s/Temporary.inp', folderPath ); 

delete( tempTextFilePath );

%% Compute CALFEM matrices from Coord and Enode

NoNodes = size( Coord, 1 );

% Compute global nodal dof matrix

Dof = zeros( NoNodes, NoNodalDofs );

for nodeIndex = 1:NoNodes
   
    Dof( nodeIndex, : ) = ( nodeIndex - 1 )*NoNodalDofs + 1 : ...
                          nodeIndex*NoNodalDofs;
    
end

% Compute topology matrix in terms of dof, Edof, and the element nodal 
% coordinates matrices Ex, Ey

NoElemDofs   = NoElemNodes*NoNodalDofs;
Edof         = zeros( NoElem, NoElemDofs + 1 );
Edof( :, 1 ) = linspace( 1, NoElem, NoElem )';

Ex = zeros( NoElem, NoElemNodes );
Ey = Ex;

for elemIndex = 1:NoElem
 
     for nodeIndex = 1:NoElemNodes

         Edof( elemIndex, 2 + ( nodeIndex - 1 )*NoNodalDofs : ...
               1 + nodeIndex*NoNodalDofs ) = ...
          Dof( Enode( elemIndex, nodeIndex ), : ); 

         Ex( elemIndex, nodeIndex ) = ...
          Coord( Enode( elemIndex, nodeIndex ), 1 );
      
         Ey( elemIndex, nodeIndex ) = ...
          Coord( Enode( elemIndex, nodeIndex ), 2 );

     end    
 
end

% Plot the mesh if desired

if strcmp( plotMesh, 'yes' )
    
    figure; 
    hold on; axis equal;
    for elemIndex = 1:NoElem
       
        plot( [ Ex( elemIndex, : ) Ex( elemIndex, 1 ) ]', ...
              [ Ey( elemIndex, : ) Ey( elemIndex, 1 ) ]', ...
              '-k' );        
        
    end
    hold off;
        
end

end

