function [T] = timestepping(T, Ktheta, CapMat, K, fbc, Edof, Ex, Ey, ...
                            Tamb, dt, theta, steps, psteps)
%function [T] = timestepping(T, Ktheta, CapMat, K, fbc, Edof, Ex, Ey, ...
%                            dt, theta, steps, psteps)
%-------------------------------------------------------------------
% Purpose: To calculate the temperature distribution at time, t, for the 
%          time discretrization chosen.
%
%-------------------------------------------------------------------
% Input:   T = previous temperature distribution
%          Ktheta = LHS of fem equation
%          CapMat = Capacity matrix of the fem formulation
%          K = stiffness matrix of the fem formulation
%          dt = time step
%          theta = based on time stepping method
%          steps = number of time steps in total
%          psteps = at which time steps to plot temperature distribution
%-------------------------------------------------------------------
% Output:  T = temperature distribution at current time step 
%
%-------------------------------------------------------------------
% Created by: James Koch and Da Yuan 2018-12-04
%-------------------------------------------------------------------

% initialize subplot counter
kk = 1;

% loop for all time steps
for i = 1:steps
    ftheta = (CapMat - dt * (1 - theta) * K) * T(:, i) + ...
              dt * (1 - theta) * fbc + ...
              dt * theta * fbc;
    T(:, i+1) = Ktheta\ftheta;
end

end