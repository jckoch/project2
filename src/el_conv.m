function [Kce, fce] = el_conv(ex, ey, Tamb, thick, alpha)
% function [ Kce, fce ] = el_conv( ex, ey, alpha, thick, Tamb )
%-------------------------------------------------------------------
% Purpose: Compute the element stiffness matrix and force vector
%          due to convection for a linear boundary segment
%-------------------------------------------------------------------
% Input: Tamb    Ambient temperature at the boundary segment, [degC]
%        thick   Out-of-plane thickness of the continuum
%        alpha   Coefficient of thermal expansion, [W/(m^2*degC)]
%        ex      Element nodal x-coordinate, size(ex) = [1, 2]
%        ey      Element nodal y-coordinate, size(ey) = [1, 2]
%-------------------------------------------------------------------
% Output: Kce    Element stiffness matrix due to convection
%         fce    Element force vector due to convection
%-------------------------------------------------------------------
% Created by: Martin Fagerström 2018-11-19
% Modified by: James Koch and Da Yuan 2018-12-04
%-------------------------------------------------------------------

dx = ex(2) - ex(1);
dy = ey(2) - ey(1);
Le = sqrt(dx^2 + dy^2);

Kce = (1/6) * Le * thick * alpha * [2 1; 1 2];

fce = (1/2) * Le * thick * alpha * Tamb * [1; 1];

end

    

