%% Plotting
tauFe = linspace(0, 5*3600, size(Tfe, 2))';
tauBe = linspace(0, 5*3600, size(Tbe, 2))';
tauCn = linspace(0, 5*3600, size(Tcn, 2))';

tf = (boundaryEx(:,1) == 0.618) & (boundaryEy(:,1) == 0);
dof = boundaryEdof(tf, 2);

figure(4);
hold on;
plot(tauFe, Tfe(dof, :), 'LineWidth', 2);
plot(tauBe, Tbe(dof, :), 'LineWidth', 2);
plot(tauCn, Tcn(dof, :), 'LineWidth', 2);
legend('FE','BE', 'CN');
xlabel('Time [s]');
ylabel('Temperature [C]');
title(sprintf('Temperature variation as a function of time for dof = %d @ x = 0.618m and y = 0m',dof));
savefig(4, sprintf('./imgs/compare3methods_%d_%s.fig', dof, meshFileName(1:end-7)));
saveas(4, sprintf('./imgs/compare3methods_%d_%s', dof, meshFileName(1:end-7)), 'epsc');