function [Kc, fb] = convectiveKc(boundaryEdof, boundaryMaterial, ex, ey, ...
                                 Tamb, alpha, thick, Dof)
% function [Kc, fb] = convectiveKc(boundaryEdof, boundaryMaterial, ...
%                               ex, ey, Tamb, alpha, thick)
%-------------------------------------------------------------------
% Purpose: Compute the convective stiffness matrix and force vector
%          due to convection for a linear boundary segment
%-------------------------------------------------------------------
% Input: Tamb               Ambient temperature at the boundary segment, [degC]
%        thick              Out-of-plane thickness of the continuum
%        alpha              Coefficient of thermal expansion, [W/(m^2*degC)]
%        ex                 Element nodal x-coordinate, size(ex) = [1, 2]
%        ey                 Element nodal y-coordinate, size(ey) = [1, 2]
%        boundaryMaterial   material index for each element
%        boundaryEdof       boundary topology matrix
%        Dof                number of dofs in the system
%-------------------------------------------------------------------
% Output: Kc                stiffness matrix due to convection
%         fc                boundary force vector due to convection
%-------------------------------------------------------------------
% Created by: James Koch and Da Yuan 2018-12-04
%-------------------------------------------------------------------

Kc = zeros(size(Dof, 1));       % global convective stiffness matrix, Kc
fb = zeros(size(Dof, 1), 1);   % global boundary load vector, fb

for i = 1:length(boundaryEdof)
    % determine which boundary
    if boundaryMaterial(i, 2) == 1 % inside boundary
        j = 1;
        % determine convective element stiffness matrix
        [Kce, fbe] = el_conv(ex(i, :), ey(i, :), Tamb(j), alpha(j), thick);
    elseif boundaryMaterial(i, 2) == 2 % outside boundary
        j = 2;
        % determine convective element stiffness matrix
        [Kce, fbe] = el_conv(ex(i, :), ey(i, :), Tamb(j), alpha(j), thick);
    else
        Kce = zeros(2);
        fbe = zeros(2, 1);
    end
    
    % assemble Kce into K
    [Kc, fb] = assem(boundaryEdof(i, :), Kc, Kce, fb, fbe);
end

end