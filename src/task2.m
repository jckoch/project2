% Project 2: Task 2 (2D Steady-state Heat Transfer)
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%   
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all;
clear all; clc;

%% define general parameters
% define wall material thickness
h = [0.4, 0.2, 0.018];

% define material thermal conductivities
k = [1.7, 0.04, 0.22];

% define convective boundary condition data
Tamb = [25, -5];
alpha = [8, 25];

% number of nodal Dof
NoNodalDofs = 1;

% define length of the wall (away from thermal bridge)
% -- NOTE -----------------------------------------------------------------
% this should be the same as the length when exporting mesh from COMSOL
% -------------------------------------------------------------------------
L = 3.5; %m

%% import 2D mesh from COMSOL
% this uses pre-defined MatLAB routines

% define default values to use in COMSOL2CALFEM_heat2D
meshFileName = 'task2_coarse_v2.mphtxt';

% define whether plots should be displayed
plotMesh = 'yes';
plotBounds = 'no';

% convert COMSOL mesh to CALFEM mesh
[Coord, Edof, Dof, Ex, Ey, matrlIndex, boundaryEdof, boundaryEx, ...
    boundaryEy, boundaryMaterial] = COMSOL2CALFEM_heat2D(meshFileName, ...
    1, h(1), h(2), h(3), 3.5, plotMesh, plotBounds);

%% define loading data
Q = 0;   % NO external heat supply
eq = Q;

%% define unit height of wall
thick = 1;

%% initialize matrices
K = zeros(size(Dof, 1));        % global stiffness matrix, K
fl = zeros(size(Dof, 1), 1);    % load vector, fl
Kc = zeros(size(Dof, 1));       % global convective stiffness matrix, Kc
fb = zeros(size(Dof, 1), 1);    % global boundary load vector, fb

%% calculate the element stiffness matrix and assemble into K
[~, n] = size(Edof); 
for i = 1:length(Edof)
    % determine the thermal conductivity of the element, i
    kk = k(matrlIndex(i, 2));
    D = [kk 0; 0 kk];
    
    % determine the element stiffness matrix and load vector
    [Ke, fe] = flw2te(Ex(i,:), Ey(i, :), thick, D, eq);
    
    % assemble Ke into K
    [K, fl] = assem(Edof(i, :), K, Ke, fl, fe);
end

%% calculate the convective element stiffness matrix
for i = 1:length(boundaryEdof)
    % determine which boundary
    if boundaryMaterial(i, 2) == 1 % inside boundary
        j = 1;
        % determine convective element stiffness matrix
        [Kce, fbe] = el_conv(boundaryEx(i, :), boundaryEy(i, :), Tamb(j), alpha(j), thick);
    elseif boundaryMaterial(i, 2) == 2 % outside boundary
        j = 2;
        % determine convective element stiffness matrix
        [Kce, fbe] = el_conv(boundaryEx(i, :), boundaryEy(i, :), Tamb(j), alpha(j), thick);
    else
        Kce = zeros(n-2);
        fbe = zeros(n-2, 1);
    end
    
    % assemble Kce into K
    [Kc, fb] = assem(boundaryEdof(i, :), Kc, Kce, fb, fbe);
end

%% Solution of FE-form
% determine combined global stiffness matrix
K = K + Kc;

% determine global load vector
f = fl + fb;

% solve the equation
T = solveq(K,f);

%% Post-processing of solution
temp = extract(Edof, T);

% Plot of Temperature distribution
figure(1); 
hold on; axis equal;
colorbar;
for elemIndex = 1:length(Edof)

    fill( [ Ex( elemIndex, : ) Ex( elemIndex, 1 ) ]', ...
          [ Ey( elemIndex, : ) Ey( elemIndex, 1 ) ]', ...
          [ temp(elemIndex, :) temp(elemIndex, 1) ]');        
        
end
xlabel('x coordinate [m]');
ylabel('y coordinate [m]');
title('Temperature distribution through the 2D wall');
hold off;

savefig(1, './imgs/task2.fig');
saveas(1, './imgs/task2', 'epsc');

%% Determine the heat outflux on the exterior wall boundary
boundaryTemp = extract(boundaryEdof, T);
q = 0; % initialize heat flux through outside boundary
for i = 1:size(boundaryEdof, 1)
    % calculate length of boundary edge of element
    dx = boundaryEx(i, 2) - boundaryEx(i, 1);
    dy = boundaryEy(i, 2) - boundaryEy(i, 1);
    Le = sqrt(dx^2 + dy^2);
    
    % calculate heat outflux accross the outside boundary
    if boundaryMaterial(i, 2) == 2 % outside boundary
        j = 2;
        qe = alpha(j) * thick * Le * (((boundaryTemp(i, 1) + boundaryTemp(i, 2))/2) - Tamb(j));
    else
        continue;
    end
    q = qe + q;
end

fprintf('Heat outflux for outside wall boundary: %.3f W/m.\n', q);
Qbar = 5.472; % W/m^2
psi = (q - Qbar*L)/(Tamb(1) - Tamb(2));
fprintf('The linear thermal transmittance of the thermal bridge: %.3f\n', psi);
psi_task1 = 0.0137;
diffq = ((psi_task1 - psi)/psi)*100;
fprintf('The percent difference from Task 1 of the linear thermal transmittance: %2.0f%%\n',diffq)

