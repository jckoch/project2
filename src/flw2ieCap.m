function [ elemCapMat ] = flw2ieCap( elemType, ex, ey, specHeatCap, NoIntPoints )
% function [ elemCapMat ] = flw2ieCap( elemType, ex, ey, specHeatCap, ..., NoIntPoints ) 
%-------------------------------------------------------------------------
% Purpose: Compute the element heat capacity matrix. Supported elements:
%          a) bilinear quadrilateral and b) linear triangular
%-------------------------------------------------------------------------
% Input: elemType     = 'quad' or 'triangular' for bilinear quadrilateral
%                     or linear triangular elements, respectively
%        ex           Element nodal x-coordinates,
%                     size(ex) = [ 1, 4 ] for bilinear quadrilaterals
%                     size(ex) = [ 1, 3 ] for linear triangles
%        ey           Element nodal y-coordinates,
%                     size(ey) = [ 1, 4 ] for bilinear quadrilaterals
%                     size(ey) = [ 1, 3 ] for linear triangles
%        specHeatCap  Specific heat capacity (rho*Cp) [J/(m^3*degC)]                     
%        NoIntPoints  Number of integration points, you may choose:
%                     1, 2 or 3 (in each direction) for bilinear quadrilaterals
%                     1, 3 or 4 for linear triangles
%-------------------------------------------------------------------------
% Output: elemCapMat  Element heat capacity matrix
%-------------------------------------------------------------------------
% Created by: Dimosthenis Floros, 20171115
%-------------------------------------------------------------------------

%% For bilinear quadrilateral elements

if strcmp( elemType, 'quad' ) == 1
    
    % Compute total number of integration points (accounting both directions)

    NoIntPointsX2 = NoIntPoints^2;

    % Gauss quadrature integration points and weights

    w1 = 5/9;
    w2 = 8/9;

    if NoIntPointsX2 == 1

         intPoints  = [ 0 0 ];
         intWeights = [ 2 2 ];

    elseif NoIntPointsX2 == 4

         intPoints  = [ -1/sqrt(3) -1/sqrt(3);
                         1/sqrt(3) -1/sqrt(3); 
                         1/sqrt(3)  1/sqrt(3);
                         -1/sqrt(3) 1/sqrt(3)];

         intWeights = [ 1 1; 1 1; 1 1; 1 1 ];

    elseif NoIntPointsX2 == 9

         intPoints = [ -sqrt(3/5)  -sqrt(3/5);
                        0         -sqrt(3/5);
                        sqrt(3/5)  -sqrt(3/5);
                        -sqrt(3/5)  0;
                        0          0;
                        0         -sqrt(3/5);
                       -sqrt(3/5)   sqrt(3/5);
                        0          sqrt(3/5);
                        sqrt(3/5)   sqrt(3/5)];

         intWeights = [ w1 w1;
                        w2 w1;
                        w1 w1;
                        w1 w2;
                        w2 w2;
                        w1 w2;
                        w1 w1;
                        w2 w1;
                        w1 w1];

    else
        
        disp( 'Used number of integration points not implemented!' );

     return

    end

    % Initialize element heat capacity matrix

    elemCapMat = zeros( 4 );

    % Loop over the integration points and compute the element heat capacity
    % matrix

    for indexIP = 1:NoIntPointsX2

        ksi = intPoints( indexIP, 1 );
        eta = intPoints( indexIP, 2 );
        weightKsi = intWeights( indexIP, 1 );
        weightEta = intWeights( indexIP, 2 );

        N1e =  1/4*( ksi - 1 )*( eta - 1 );
        N2e = -1/4*( ksi + 1 )*( eta - 1 );
        N3e =  1/4*( ksi + 1 )*( eta + 1 );
        N4e = -1/4*( ksi - 1 )*( eta + 1 );

        Ne = [ N1e N2e N3e N4e ];

        dN1edKsi =  1/4*( eta  - 1   );  dN1edEta = 1/4*( ksi - 1 );
        dN2edKsi =  1/4*( 1    - eta );  dN2edEta = 1/4*( -ksi - 1 );
        dN3edKsi =  1/4*( eta  + 1   );  dN3edEta = 1/4*( ksi + 1 );
        dN4edKsi =  1/4*( -eta - 1   );  dN4edEta = 1/4*( 1 - ksi );

        dNedKsi = [ dN1edKsi dN2edKsi dN3edKsi dN4edKsi];
        dNedEta = [ dN1edEta dN2edEta dN3edEta dN4edEta];

        J = [ dNedKsi*ex' dNedEta*ex';
            dNedKsi*ey' dNedEta*ey'];

        detJ = det(J);

        if detJ < 10*eps
            
            disp('Jacobideterminant equal or less than zero!');
            
        end

        elemCapMat = elemCapMat + ...
        specHeatCap*Ne'*Ne*detJ*weightKsi*weightEta;     

    end

%% For linear triangular elements
   
elseif strcmp( elemType, 'triangular' ) == 1
    
       % Gauss quadrature integration points and weights

        if NoIntPoints == 1

           L1 = 1/3; L2 = 1/3; L3 = 1/3;
           intWeights = 1;

        elseif NoIntPoints == 3

           L1 = [ 1/2 1/2 0 ]; L2 = [ 1/2 0 1/2 ]; L3 = [ 0 1/2 1/2 ];
           intWeights = [ 1/3 1/3 1/3 ];

        elseif NoIntPoints == 4

           L1 = [ 1/3 0.6 0.2 0.2 ]; L2 = [ 1/3 0.2 0.6 0.2 ]; 
           L3 = [ 1/3 0.2 0.2 0.6 ];
           intWeights = [ -27/48 25/48 25/48 25/48 ];

        else

             L1 = 0; L2 = 0; L3 = 0;
             intWeights = 0;
             disp( 'Used number of integration points not implemented!' );

        end

        intPoints = [ L1' L2' L3' ]; % Area coordinates
       
        % Initialize element heat capacity matrix

        elemCapMat = zeros( 3 );

        % Loop over the integration points and compute the element heat capacity
        % matrix

        for indexIP = 1:NoIntPoints

            L1       = intPoints( indexIP, 1 );
            L2       = intPoints( indexIP, 2 );
            L3       = intPoints( indexIP, 3 );
            weightIP = intWeights( indexIP );
            
            % Write element shape functions

            Ne = [ L1 L2 L3 ];
            
            % Compute the Jacobian of the isopar transformation

            ex_Cross_ey = [ ex(2) - ex(1), ex(3) - ex(1);
                            ey(2) - ey(1), ey(3) - ey(1) ];                 

            trArea = 1/2*abs( det( ex_Cross_ey ) ); 

            elemCapMat = elemCapMat + specHeatCap*Ne'*Ne*trArea*weightIP;     

        end
    
else 
    
    disp( [ 'Chosen element not implemented. Check again the value of' ...
            'string "elemType".' ] );
        
end

end