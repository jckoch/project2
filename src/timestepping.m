function [T] = timestepping(T, Ktheta, CapMat, K, fbc, Edof, Ex, Ey, ...
                            Tamb, dt, theta, steps, psteps)
%function [T] = timestepping(T, Ktheta, CapMat, K, fbc, Edof, Ex, Ey, ...
%                            dt, theta, steps, psteps)
%-------------------------------------------------------------------
% Purpose: To calculate the temperature distribution at time, t, for the 
%          time discretrization chosen.
%
%-------------------------------------------------------------------
% Input:   T = previous temperature distribution
%          Ktheta = LHS of fem equation
%          CapMat = Capacity matrix of the fem formulation
%          K = stiffness matrix of the fem formulation
%          dt = time step
%          theta = based on time stepping method
%          steps = number of time steps in total
%          psteps = at which time steps to plot temperature distribution
%-------------------------------------------------------------------
% Output:  T = temperature distribution at current time step 
%
%-------------------------------------------------------------------
% Created by: James Koch and Da Yuan 2018-12-04
%-------------------------------------------------------------------

% initialize subplot counter
kk = 1;

% loop for all time steps
for i = 1:steps
    ftheta = (CapMat - dt * (1 - theta) * K) * T(:, i) + ...
              dt * (1 - theta) * fbc + ...
              dt * theta * fbc;
    T(:, i+1) = Ktheta\ftheta;
    if i == 1
        ed = extract(Edof, T(:, i));
        subplot(1, length(psteps)+2, kk);
        fill(Ex', Ey', ed');
        colormap jet;
        caxis(fliplr(Tamb));
        colorbar;
        title(sprintf('t = 0 hr'));
        xlabel('x-coordinate [m]');
        ylabel('y-coordinate [m]');
    elseif psteps(kk) == i
        kk = kk + 1;
        ed = extract(Edof, T(:, i+1));
        subplot(1, length(psteps)+2, kk);
        fill(Ex', Ey', ed');
        colormap jet;
        caxis(fliplr(Tamb));
        colorbar;
        title(sprintf('t = %.2f hr', (dt*i)/3600));
        xlabel('x-coordinate [m]');
        ylabel('y-coordinate [m]');
    elseif i == steps
        ed = extract(Edof, T(:, i+1));
        subplot(1, length(psteps)+2, kk);
        fill(Ex', Ey', ed');
        colormap jet;
        caxis(fliplr(Tamb));
        colorbar;
        title(sprintf('time = %1.0f hr', (dt*i)/3600));
        xlabel('x-coordinate [m]');
        ylabel('y-coordinate [m]');
    end
end

end